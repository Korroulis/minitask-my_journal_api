/////  Definition of needed consts

const elEntries= document.getElementById('entries');
const elJournals= document.getElementById('journals');
const elJournalSelect= document.getElementById('journalSelect');
const elAuthorSelect= document.getElementById('authorSelect');

///// AJAX post request

$(document).ready(function() {

  console.log('page ready')

  $('#entryPost').submit(function(event) {
    event.preventDefault();

    const newEntry = {
      title: $('#EntryTitle').val(),
      content: $('#EntryContent').val(),
      //journal_id: $('#journalSelect').val()[0],
      journal_id: Math.floor(elJournalSelect.selectedIndex+1),
      author_id: Math.floor(elAuthorSelect.selectedIndex+1)
    };
  console.log(newEntry);
          // process the form
          $.ajax({
            type        : 'POST', 
            url         : 'http://localhost:3000/v1/api/entries', // the url where we want to POST
            data        : newEntry, 
            dataType    : 'json', 
                        encode          : true
        })
            // using the done promise callback
            .done(function(data) {
                // log data to the console so we can see
                console.log(data); 
                // window.location = '/index.html'; // redirect to the home page
                event.preventDefault();
                alert('Entry posted successfully'); 
            });
        window.location = '/index.html'; // redirect to the home page after entry submission
    });

});

///// List all entries 

function entry() {
    fetch('http://localhost:3000/v1/api/entries')
    .then((response) => {
    return response.json();

  })

  .then((data) => {
    console.log(data);

       data.forEach(entry => {
        const elEntry = document.createElement("li");

        const elEntryTitle = document.createElement("h4");
        elEntryTitle.innerText = entry.title;

        const elEntryContent = document.createElement("p");
        elEntryContent.innerText = entry.content;

        elEntry.appendChild(elEntryTitle);
        elEntry.appendChild(elEntryContent);

        elEntries.appendChild(elEntry);
      
       }); 
})
}

///// List all journals 

function journal() {
  fetch('http://localhost:3000/v1/api/journals')
  .then((response) => {
  return response.json();

})
.then((data) => {
  console.log(data);

  data.forEach(journal => {
    const elJournal = document.createElement("li");
    const elJournalTitle = document.createElement("h4");
    elJournalTitle.innerText = journal.name;

    elJournal.appendChild(elJournalTitle);
    elJournals.appendChild(elJournal);
  
   }); 
})
}

/////  Create a new entry using data for the existing journals and authors for the select box

function journalEntry() {
    fetch('http://localhost:3000/v1/api/journals')
    .then((response) => {
    return response.json();

  })

  .then((data) => {
    console.log(data);

    data.forEach(journal => {
      const elSelect = document.createElement("option");
      elSelect.innerText = journal.name;
  
      elJournalSelect.appendChild(elSelect);
    
     }); 
})
.then(fetch('http://localhost:3000/v1/api/authors')
.then((response) => {
  return response.json();

})
.then((data) => {
  console.log(data);


  data.forEach(author => {
    const elAuthor = document.createElement("option");
    elAuthor.innerText = `${author.first_name} ${author.last_name}`;

    elAuthorSelect.appendChild(elAuthor);
  
   }); 
})
)
}
