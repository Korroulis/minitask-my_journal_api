const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
//const uuid = require('uuid');
const path = require('path');
const router = express.Router();

const app = express();
app.use(bodyParser.urlencoded({ extended: true}));
app.use(cors());

app.use('/public', express.static( path.join(__dirname, '../minitask_website')));

const PORT = process.env.PORT || 3000;


const { Sequelize, QueryTypes } = require('sequelize');

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

const sequelize = new Sequelize ({
    database: process.env.DB_DATABASE,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT
});

/////  Endpoint for authors

async function author() {
    try {
        await sequelize.authenticate();
        console.log('Connection established successfully');
        const authors = await sequelize.query('SELECT * FROM author', { type: QueryTypes.SELECT });
        console.log(authors);
        app.get('/v1/api/authors', (req,res)=>{
            res.send(authors);
        });
    }
    catch (e) {
        console.error(e);
    }
}

author();

/* app.get('/v1/api/journals', (req,res)=>{
    //res.send(authors);
    //res.send('Hellooo');
    res.sendFile( path.join( __dirname,'../minitask_website/journals.html'));
}); */

/////  Endpoint for journals

async function journal() {
    try {
        console.log('Connection established successfully');
        const journals = await sequelize.query('SELECT * FROM journal', { type: QueryTypes.SELECT });
        console.log(journals);
        app.get('/v1/api/journals', (req,res)=>{
            res.send(journals);
        });
    }
    catch (e) {
        console.error(e);
    }
}

journal();

/////  Endpoint for entries (new entry posted in the endpoint everytime this action is performed from the frontend)

async function journalEntries() {
    try {
        console.log('Connection established successfully');
        const journal_entries = await sequelize.query('SELECT * FROM journal_entry', { type: QueryTypes.SELECT });
        console.log(journal_entries);
        app.post('/v1/api/entries', (req,res) => {

            const newEntry = {journal_entry_id: journal_entries.length+1,
                title:req.body.title,
                content:req.body.content,
                created_at: new Date().toISOString(),
                updated_at: null,
                journal_id:parseInt(req.body.journal_id),
                author_id:parseInt(req.body.author_id)
            };         
            console.log(newEntry);
            journal_entries.push(newEntry);
            res.send(journal_entries);
        });
        app.get('/v1/api/entries', (req,res)=>{
            res.send(journal_entries);
        });
        await sequelize.close();
    }
    catch (e) {
        console.error(e);
    }
}

journalEntries();

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));