## Info on the process followed

1. Two separate folders were created (not in the same dir), named minitask_API and minitask_website. The first one contained all the files needed for the info from the database to be obtained and then stored in the server, as well as files to perform get and post actions from the desired endpoints. The latter folder contained all the html, css and js files for the website. 

2. The minitask_API was ran from the terminal using node express and subsequently the index.html file was ran with the live server. The from the navbar the user can redirect to the other pages, such as the one that lists all the journals and the one that opens up the page to create a new entry. When a new entry is created the user is redirected to the home page (list all entries page), which is updated with any new inserted entries

## !!!!! Unable to get the html files running properly from the browser (e.g. trying http://localhost:3000/v1/api/journals) app.get cannnot be performed as error messages are logged. Will try to solve that when i have time

## Task description

# My Journal API

## 1. Create the database using MySQL workbench

## 2. Create a Node Express API with the following endpoints
      a. Get a list of journals
      b. Get a list of journal entries
      c. Add a journal entry

## 3. Create a website with the following pages
      a. index.html
         * List the journal entries
      b. journals.html
         * List the journals
      c. create-entry.html
         * A page that can add a journal entry
         * author_id and journal_id must be added
         * Add a select box to pick a journal and author from the respective pools for the journal entry to be added